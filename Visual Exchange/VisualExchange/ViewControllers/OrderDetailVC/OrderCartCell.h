//
//  OrderCartCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 10/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lbldetail;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblprice;
@property(assign,nonatomic) NSInteger from_sellonVC;
@property (weak, nonatomic) IBOutlet UILabel *lblcoupon;

@end
