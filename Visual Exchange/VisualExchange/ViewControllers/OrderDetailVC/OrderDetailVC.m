//
//  OrderDetailVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "OrderDetailVC.h"

@interface OrderDetailVC ()<UITableViewDelegate>
{
    NSMutableArray *arrayorder;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
       // Do any additional setup after loading the view from its nib.
    arrayorder = [self.result[@"productarray"]mutableCopy];
    self.lbltitle.text = [NSString stringWithFormat:@"Order # %@",self.result[@"order_id"]];
    self.tableview.backgroundColor = [UIColor clearColor];
    
   // UINib *nib = [UINib nibWithNibName:@"OrderDetailCell" bundle:nil];
   // if(_from_sellonVC == 1){
      UINib*  nib = [UINib nibWithNibName:@"SellOnVECell" bundle:nil];
   // }
    [self.tableview registerNib:nib forCellReuseIdentifier:@"OrderDetailCell"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(!arrayorder.count){
        return 0;
    }
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return arrayorder.count;
    }
   
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(indexPath.section == 0){
        
        OrderCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCartCell" forIndexPath:indexPath];
        cell.lblName.text = arrayorder[indexPath.row][@"name"];
        cell.lbldetail.text = arrayorder[indexPath.row][@"categoryname"];
        cell.lblQty.text = [NSString stringWithFormat:@"Qty : %@", arrayorder[indexPath.row][@"qty"]];
        if( [arrayorder[indexPath.row][@"code"] isEqualToString:@""])
        {
            cell.lblcoupon.hidden=YES;
        }
        else
        {
            cell.lblcoupon.hidden=NO;
            
            
            if( [[arrayorder[indexPath.row][@"discountType"]stringValue] isEqualToString:@"2"])
            {
                NSString *offstr=@"%off";
                cell.lblcoupon.text = [NSString stringWithFormat:@"Coupon Applied: %@ (%@ %@)", arrayorder[indexPath.row][@"code"],arrayorder[indexPath.row][@"discount"],offstr];
            }
            else
            {
                cell.lblcoupon.text = [NSString stringWithFormat:@"Coupon Applied: %@ ($%@ discount)", arrayorder[indexPath.row][@"code"],arrayorder[indexPath.row][@"discount"]];
            }
        }
        

        cell.lblprice.text = [NSString stringWithFormat:@"USD : %@", arrayorder[indexPath.row][@"price"]];
        cell.backgroundColor = [UIColor clearColor];
       [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136.png"] imageview:cell.imgProduct urlString:arrayorder[indexPath.row][@"image"] Complete:^(UIImage *image) {}];
        return cell;
    }else
    {
       
        OrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderDetailCell" forIndexPath:indexPath];
       // NSMutableArray *ary=[_result objectForKey:@"shipping_details"];
       // cell.lblname.text = [NSString stringWithFormat:@": %@",[[ary objectAtIndex:0]objectForKey:@"name"]];
       // cell.lblmobile.text = [NSString stringWithFormat:@": %@",[[ary objectAtIndex:0]objectForKey:@"phone"]];
       // cell.lblStatus.text = [NSString stringWithFormat:@": %@",[[ary objectAtIndex:0]objectForKey:@"status"]];

        
        if (![self.result[@"shipping_details"] isKindOfClass:[NSArray class]]){
            cell.lblname.text = [NSString stringWithFormat:@": %@",self.result[@"shipping_details"][@"name"]];
          cell.lblmobile.text = [NSString stringWithFormat:@": %@",self.result[@"shipping_details"][@"phone"]];
            cell.lblStatus.text = [NSString stringWithFormat:@": %@",[self ststusWithCode:self.result[@"status"]]];
            NSString *date= [NSString stringWithFormat:@"%@",self.result[@"order_date"]];
            NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
             [dtF setDateFormat:@"yyyy-MM-dd"];
            NSDate *d = [dtF dateFromString:date];
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *st = [dateFormat stringFromDate:d];
            NSLog(@"%@",st);
            
            cell.lbldate.text=[NSString stringWithFormat:@": %@",st];

            cell.lbladress.text = [NSString stringWithFormat:@": %@, %@",self.result[@"shipping_details"][@"address"],self.result[@"shipping_details"][@"city"]];
        
        
       // cell.lbladress.text= [NSString stringWithFormat:@": %@, %@",[[ary objectAtIndex:0]objectForKey:@"address"],[[ary objectAtIndex:0]objectForKey:@"city"]];
        

        
            if (_from_sellonVC == 1)
            {
               
                cell.lblTotal.text = [NSString stringWithFormat:@"$%@.00",self.result[@"totalAmount"]];
                cell.lblPromocode.text = [NSString stringWithFormat:@"- $%@.00",self.result[@"totalAmountDiscount"]];
                cell.lblTopay.text = [NSString stringWithFormat:@"$%@.00",self.result[@"totalAmountRemaining"]];
                
               
              //  cell.lblTopay.text = [NSString stringWithFormat:@"$%@.00",self.result[@"total_amount"]];
                cell.lblShiping.text = [NSString stringWithFormat:@"$%@.00",self.result[@"shipping_amount"]];
                cell.lbltax.text = [NSString stringWithFormat:@"$%@.00",self.result[@"tax"]];
              

            }else
            {
                cell.lblShiping.text = [NSString stringWithFormat:@"$%@.00",self.result[@"shipping_amount"]];
                cell.lbltax.text = [NSString stringWithFormat:@"$%@.00",self.result[@"tax"]];
                
                
                
                cell.lblTotal.text = [NSString stringWithFormat:@"$%@.00",self.result[@"totalAmount"]];
                cell.lblPromocode.text = [NSString stringWithFormat:@"- $%@.00",self.result[@"totalAmountDiscount"]];
                NSString *total=[NSString stringWithFormat:@"%@.00",self.result[@"totalAmount"]];
                NSString *totalamountdiscount=[NSString stringWithFormat:@"%@.00",self.result[@"totalAmountDiscount"]];
                int a=[total intValue]-[totalamountdiscount intValue];
                NSString *a1=[NSString stringWithFormat:@"%d",a];
                cell.lblTopay.text = [NSString stringWithFormat:@"$%@.00",a1];
                
            }
            
            if (![self.result[@"promocode"] isEqualToString:@""])
            {
               // cell.lblstaticPromocode.text =  [NSString stringWithFormat:@"Promotion(%@):",self.result[@"promocode"]];
                 cell.lblstaticPromocode.text =  @"Promotion:";
            }
            else{
                cell.lblPromocode.text =@"- $0.00";
                
                cell.lblstaticPromocode.text =@"Promotion:";
            }
            NSString *flat=[NSString stringWithFormat:@"%@",self.result[@"is_flat_rate"]];
            if([flat isEqualToString:@"0"])
            {
                cell.shippinglabel.text =@"Fedex Shipping:";
            }
            else
            {
                cell.shippinglabel.text =@"Flat Rate Shipping:";
                
                
            }
            
       }else
        {
            
            
            
            cell.lblname.text = [NSString stringWithFormat:@": %@",self.result[@"shipping_details"][0][@"name"]];
            cell.lblmobile.text = [NSString stringWithFormat:@": %@",self.result[@"shipping_details"][0][@"phone"]];
            cell.lblStatus.text = [NSString stringWithFormat:@": %@",[self ststusWithCode:self.result[@"status"]]];
            NSString *date= [NSString stringWithFormat:@"%@",self.result[@"order_date"]];
            NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
            [dtF setDateFormat:@"yyyy-MM-dd"];
            NSDate *d = [dtF dateFromString:date];
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *st = [dateFormat stringFromDate:d];
            NSLog(@"%@",st);
            
            cell.lbldate.text=[NSString stringWithFormat:@": %@",st];
            

            NSString *flat=[NSString stringWithFormat:@"%@",self.result[@"is_flat_rate"]];
            if([flat isEqualToString:@"0"])
            {
                cell.shippinglabel.text =@"Fedex Shipping:";
            }
            else
            {
                cell.shippinglabel.text =@"Flat Rate Shipping:";

                
            }
            cell.lbladress.text = [NSString stringWithFormat:@": %@, %@",self.result[@"shipping_details"][0][@"address"],self.result[@"shipping_details"][0][@"city"]];
        
            cell.lblShiping.text = [NSString stringWithFormat:@"$%@",self.result[@"shipping_amount"]];
            cell.lbltax.text = [NSString stringWithFormat:@"$%@",self.result[@"tax"]];
            cell.lblTotal.text = [NSString stringWithFormat:@"$%@",self.result[@"amount"]];
            cell.lblPromocode.text = [NSString stringWithFormat:@"- $%@",self.result[@"promocode"]];
            cell.lblTopay.text = [NSString stringWithFormat:@"$%@",self.result[@"total_amount"]];
            if (![self.result[@"couponCode"] isEqualToString:@""])
            {
               // cell.lblstaticPromocode.text =  [NSString stringWithFormat:@"Promotion(%@):",self.result[@"couponCode"]];
                
                cell.lblstaticPromocode.text =  @"Promotion:";
            }
            else{
                cell.lblPromocode.text =@"- $0.00";
                
                cell.lblstaticPromocode.text =@"Promotion:";
            }
           
}
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
       
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0){
       return 150;
      // return 110 * SCREEN_WIDTH/320;
    }
        return 380 * SCREEN_WIDTH/320;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0)
        return 45*SCREEN_WIDTH/320;
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        
        UIView *view = [UIView new];
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH-20, (45*SCREEN_WIDTH/320)-10)];
        lbl.text = @"Order Details";
        lbl.backgroundColor = [UIColor lightGrayColor];
        lbl.textColor =[UIColor whiteColor];
        lbl.font =[UIFont fontWithName:OpenSenceSB size:14];
        lbl.FontAutomatic = true;
        lbl.textAlignment = NSTextAlignmentCenter;
        [view addSubview:lbl];
        [lbl TopRedius:4];
        view.backgroundColor = [UIColor clearColor];
       
        return view;
    }
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
-(IBAction)BackPress:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
}
@end
