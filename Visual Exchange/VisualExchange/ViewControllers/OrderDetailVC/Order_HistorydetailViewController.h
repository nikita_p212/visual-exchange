//
//  Order_HistorydetailViewController.h
//  VisualExchange
//
//  Created by mac on 2/8/17.
//  Copyright © 2017 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"

@interface Order_HistorydetailViewController : BaseVC
{
    NSMutableArray *arrayorder;
}
- (IBAction)searchbtnpress:(id)sender;
- (IBAction)backpress:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;
@property (weak, nonatomic) IBOutlet UITableView *tblview;
@property(strong,nonatomic)NSMutableDictionary *result;
@end
