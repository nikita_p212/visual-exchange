//
//  OrderCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 25/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblproduct;
@property (weak, nonatomic) IBOutlet UILabel *lblUSD;
@property (weak, nonatomic) IBOutlet UILabel *lblordetID;
@end
