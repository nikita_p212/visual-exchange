//
//  OrderHistoryVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "OrderHistoryVC.h"
#import "Order_HistorydetailViewController.h"

@interface OrderHistoryVC ()
{
    NSMutableArray *arrayOrder;
}
@end

@implementation OrderHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableview.backgroundColor = [UIColor clearColor];
    _tableview.opaque = NO;
    _tableview.backgroundView = nil;
    
    UINib *nibProduct = [UINib nibWithNibName:NSStringFromClass([OrderCell class]) bundle:nil];
    [self.tableview registerNib:nibProduct forCellReuseIdentifier:@"OrderCell"];
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//http://www.thevisualexchange.com/visualweb/api/get/order_history

    dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"order_history" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        arrayOrder = [JSON[@"orderstList"]mutableCopy];
        [self.tableview reloadData];
        if (arrayOrder.count == 0){
            [self showAlertWithTitle:@"No order History Found" message:@"You have not purchased any product."];
        }
        
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mart - UITableview Delegate -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayOrder.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
    cell.lblDate.text = [NSString stringWithFormat:@": %@",arrayOrder[indexPath.row][@"order_date"]];
    cell.lblordetID.text = [NSString stringWithFormat:@"Order ID: #%@", arrayOrder[indexPath.row][@"order_id"]];
    cell.lblproduct.text =[NSString stringWithFormat:@": %lu",[arrayOrder[indexPath.row][@"productList"]count]];
    cell.lblStatus.text = [NSString stringWithFormat:@": %@", [self ststusWithCode:arrayOrder[indexPath.row][@"status"]]];
    cell.lblUSD.text = [NSString stringWithFormat:@": %@",arrayOrder[indexPath.row][@"paid_amount"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Order_HistorydetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"Order_HistorydetailViewController"];
    obj.result = [arrayOrder[indexPath.row]mutableCopy];
    [self.navigationController pushViewController:obj animated:true];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 110*SCREEN_WIDTH/320;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
    
}
- (IBAction)MenuPress:(id)sender {
    [[AppLauncher sharedInstance]openLeftDrawer];
}



@end
