//
//  BaseVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = false;
       
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods
- (void)showErrorWithMessage:(NSString *)errorMessage
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    alert.delegate=self;
    [alert show];
}

#pragma mark - Shake Event
- (void)motionBegan:(UIEventSubtype)motion withEvent:(nullable UIEvent *)event
{
    if (!LockedMode) {
        [self onShakeEventOccurred];
    }
}

- (void)onShakeEventOccurred {
    
}
-(CGFloat)DDD:(float)floating{
    
    return floating * SCREEN_WIDTH/320;
}
-(NSString *)ststusWithCode:(id)status{
    
    NSInteger ststuscode = [status integerValue];
    if (ststuscode == 0){
        return @"Pending";
        
    }else if (ststuscode == 1){
         return @"In process";
    }
    else if (ststuscode == 2){
         return @"Completed";
    }
    else if (ststuscode == 3){
         return @"Order cancel";
    }
    else if (ststuscode == 4){
         return @"Payment fail";
    }
     return @"";
}
- (IBAction)DLSearchBtClicked:(id)sender {
    [DLHelper AppDelegate].tabbarController.selectedIndex = 2;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
    UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SearchCatagoryVC"];
    navigation.viewControllers = @[leftDrawer].mutableCopy;
    
}
@end
