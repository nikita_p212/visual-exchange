//
//  CategoryTableViewCell.h
//  VisualExchange
//
//  Created by mac on 12/21/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkimg;
@property (weak, nonatomic) IBOutlet UILabel *catlbl;
@property (weak, nonatomic) IBOutlet UIButton *chekbtn;

@end
