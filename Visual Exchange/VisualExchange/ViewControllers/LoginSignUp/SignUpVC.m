//
//  SignUpVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "SignUpVC.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface SignUpVC () <UITextFieldDelegate>
{
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword1;
    __weak IBOutlet UITextField *tfname;
    __weak IBOutlet UITextField *tfPhone;
    __weak IBOutlet UIButton *btSignUp;
//    __weak IBOutlet UIScrollView *theScrollView;
    
    IBOutlet TPKeyboardAvoidingScrollView *theScrollView;
    NSString *emailTextToValidate;
    UIView *selectedView;
}
@end
#define MAX_LENGTH 10
@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Singup button shape
    [btSignUp layoutIfNeeded];
    CGRect frm=btSignUp.frame;
    NSLog(@" Height of btn %f",frm.size.height);
    btSignUp.layer.cornerRadius =frm.size.height/2;
    btSignUp.layer.masksToBounds=YES;

    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
    style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexBarButton,doneButton, nil]];
    tfPhone.inputAccessoryView = keyboardDoneButtonView;
    
    //set text field delegates
}
- (IBAction)doneClicked:(id)sender
{
    [tfPhone resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self addKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   // [self removeKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard notifications
//- (void)addKeyboardNotifications
//{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
//}
//
//- (void)removeKeyboardNotifications
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
//}
//
//- (void)keyboardDidShow:(NSNotification *)notif
//{
//    if (!selectedView)
//    {
//        return;
//    }
//    NSDictionary* info = [notif userInfo];
//    
//    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    CGPoint buttonOrigin = [selectedView convertPoint:selectedView.frame.origin toView:nil];
//    
//    CGFloat buttonHeight = selectedView.frame.size.height;
//    
//    CGRect visibleRect = self.view.frame;
//    
//    visibleRect.size.height -= keyboardSize.height;
//    
//    if (!CGRectContainsPoint(visibleRect, buttonOrigin))
//    {
//        
//        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
//        [theScrollView setContentOffset:scrollPoint animated:YES];
//    }
//}

- (void)keyboardDidHide:(NSNotification *)notif
{
    [theScrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - Actions
- (IBAction)onBackBtClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSignUpBtClicked:(id)sender
{
    
    [self.view endEditing:YES];
    NSString *errorString = nil;
    
    if ([tfname.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter user name." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([tfEmail.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (!tfEmail.text.validateEmailWithString)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([tfPassword1.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (tfPassword1.text.length<6)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Password should be at least 6 characters long." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_txt_reEnterPassword.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter Re-enter password." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![_txt_reEnterPassword.text isEqualToString:tfPassword1.text])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid Re-enter password." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
           [alert show];
    }
    else if ([tfPhone.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid phone number." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (tfPhone.text.length<10)
    {
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Phone number should be at least 10 characters long." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ( tfPhone.text.length>10)
    {
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Phone number should be only 10 characters long." delegate:sender cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
//     if (tfname.text.isEmptyString)
//     {
//         errorString = @"Please enter User Name";
//     }
//     else if (!tfEmail.text.validateEmailWithString)
//     {
//        
//        errorString = @"Email address is not valid.";
//        
//     }
//     else if (!tfPassword1.text.validatePassword)
//    {
//        errorString = @"Password should be at least 6 characters long";
//    }
//    else if (!tfPhone.text.validatePhoneNumber)
//    {
//        errorString = @"Phone number is not valid.";
//        
//    }
//        else if ([self.imgprofile.image isEqual:[UIImage imageNamed:@"demo_image"]])
//    {
//        [self showErrorWithMessage:dlkimageProfile];
//    }
    else
    {
       

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"email"] = tfEmail.text;
         dict[@"password"] = tfPassword1.text;
         dict[@"phone"] = tfPhone.text;
         dict[@"name"] = tfname.text;
        dict[@"image"] = self.imgprofile.image;
        NSMutableArray *cartary=[[NSUserDefaults standardUserDefaults]objectForKey:@"cartary"];
        NSString *str =[NSString stringWithFormat:@"%@",cartary];
        NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSString *str1 =[s stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *laststr =[str1 stringByReplacingOccurrencesOfString:@"()" withString:@""];
        
        
        NSString *str2 = [laststr stringByReplacingOccurrencesOfString:@"(" withString:@""];
        NSString *finalstr = [str2 stringByReplacingOccurrencesOfString:@")" withString:@""];
        dict[@"cart_ids"] = finalstr;
               

        
        
        [[DLHelper shareinstance]DLSERVICE_IMAGE:self.view Indicater:true url:@"registration.php" UrlBody:dict Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            NSString *MSG=[JSON objectForKey:@"messages"];
            if ([MSG isEqualToString:@"This email already registered."])
            {
                UIAlertView *objalert=[[UIAlertView alloc]initWithTitle:@"" message:@"This email already registered." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [objalert show];
            }
            
            [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
                [self.navigationController popViewControllerAnimated:true];
            } title:nil message:JSON[@"message"] canceltile:nil othertitle:@"Ok", nil];
            
           
//            for(NSString *str in JSON.allKeys){
//                if([JSON[str] isKindOfClass:[NSNull class]]){
//                    JSON[str] = @"";
//                }
//            }
//            JSON[@"customer_id"] = JSON[@"user_id"];
//            [JSON removeObjectForKey:@"user_id"];
//            kUserDefults(@"", ISLOGIN);
//            kUserDefults(JSON.mutableCopy, ResultUser);
//            [[DLHelper AppDelegate]isLoginUser];
           

        }];
    }
    
   // if(errorString!=nil){
       // [self showErrorWithMessage:errorString];
   // }
    
    
    
   
    
}

#pragma mark - UITextFieldDelegate


- (void)textFieldDidBeginEditing:(UITextField *)textField{
    selectedView = textField;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tfname)
    {
        [tfEmail becomeFirstResponder];
    } else if (textField == tfEmail)
    {
        [tfPassword1 becomeFirstResponder];
    }
    else if (textField == tfPassword1)
    {
        [_txt_reEnterPassword becomeFirstResponder];
    }
    else if (textField == _txt_reEnterPassword)
    {
        [tfPhone becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@" TEXT %@",tfname.text);
//    if (range.length==1 && string.length==0)
//        
//
    if (textField==tfPhone)
    {
        if (textField.text.length <= 15)
        {
            return YES;
        }
        
    }
    return YES;
}
#pragma mark - Validations

- (void)updateButtonStateWithState:(BOOL)state
{
    if (state)
    {
       // btSignUp.backgroundColor = [UIColor specSheetGreen];
    } else
    {
      //  btSignUp.backgroundColor = [UIColor lightGrayColor];
    }
}

#pragma mark - on Shake Event
- (void)onShakeEventOccurred
{
//    tfEmail.text = @"minhaz.panara@gmail.com";
//    emailTextToValidate = tfEmail.text;
//    tfPassword1.text = @"12345678";
//    tfPassword2.text = @"12345678";
//    tfPhone.text = @"1234567890";
    
}

#pragma mark - Touch Events
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (IBAction)Tapdetect:(id)sender {
    
    
  
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose image from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Photo Gallery",nil];
    [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) { // Camera
        [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera];
    } else  if (buttonIndex == 1) { // Photo Gallery
        [self openImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

- (void)openImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        UIImagePickerController *ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = sourceType;
        ipc.allowsEditing = NO;
        [self presentViewController:ipc animated:YES completion:nil];
    }];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *tmp = nil;
    if (info[UIImagePickerControllerEditedImage]) {
        tmp = info[UIImagePickerControllerEditedImage];
    } else {
        tmp = info[UIImagePickerControllerOriginalImage];
    }
    NSData *imgData = UIImageJPEGRepresentation(tmp, 1);
    NSLog(@"Size of Image(bytes):%lu",(unsigned long)[imgData length]);
    NSInteger sizeInMB = imgData.length/(1024*1024);
    if (sizeInMB <= 5) {
        UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
        self.imgprofile.image = img;
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Maximum image size is 5 MB" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}

@end
