//
//  CartCell.m
//  VisualExchange
//
//  Created by Nilay Shah on 23/04/1938 Saka.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "CartCell.h"

@implementation CartCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)PlusPress:(id)sender {
    
    if(self.didtapBlock){
        self.didtapBlock(sender);
    }
    NSInteger count = self.lblQuantity.text.integerValue;
   
        count = count+1;
        self.lblQuantity.text = [NSString stringWithFormat:@"%ld",count];

}
- (IBAction)MinusPress:(id)sender {
    
    
   
    NSInteger count = self.lblQuantity.text.integerValue;
    if (count > 1) {
        if(self.didtapBlock){
            self.didtapBlock(sender);
        }
        count = count - 1;
       self.lblQuantity.text = [NSString stringWithFormat:@"%ld",count];
    }
}
- (IBAction)DeletePress:(UIButton *)sender {
    
    if (sender.tag==4){
        if(self.didtapBlock){
            self.didtapBlock(sender);
        }
        return;
    }
    
    
   // [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
     //   if(DLindex == 1){
           
        //    if(self.didtapBlock){
                self.didtapBlock(sender);
          //  }
      ///  }
        
  //  } title:@"VisualExchange" message:@"are you sure you want to delete?" canceltile:@"Cancel" othertitle:@"Delete", nil];

   
}




@end
