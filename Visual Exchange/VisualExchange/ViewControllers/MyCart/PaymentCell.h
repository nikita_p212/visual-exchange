//
//  PaymentCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 23/04/1938 Saka.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *upperview;
@property (weak, nonatomic) IBOutlet UIButton *labcontinuetn;
@property (weak, nonatomic) IBOutlet UILabel *lblmaintax;
@property (weak, nonatomic) IBOutlet UIView *bootomviewlbkl;
@property (weak, nonatomic) IBOutlet UILabel *lblpromotion;
@property (weak, nonatomic) IBOutlet UITextField *txtPromocode;
@property (weak, nonatomic) IBOutlet UILabel *lblmainpay;
-(void)setDidtapBlock:(void(^)(UIButton *button))compition;
@property(strong,nonatomic)void(^didtapBlock)(UIButton *button);
@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (weak, nonatomic) IBOutlet UILabel *lbltotalprice;
@property (weak, nonatomic) IBOutlet UILabel *lblpromocode;
@property (weak, nonatomic) IBOutlet UILabel *lblmainpromotion;
@property (weak, nonatomic) IBOutlet UILabel *lbltax;
@property (weak, nonatomic) IBOutlet UILabel *lblShiping;
@property (weak, nonatomic) IBOutlet UILabel *lblamount;
@property (weak, nonatomic) IBOutlet UILabel *lblshipping;
@property (weak, nonatomic) IBOutlet UILabel *lbltotal;
@end
