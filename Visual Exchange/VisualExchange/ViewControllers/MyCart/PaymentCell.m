//
//  PaymentCell.m
//  VisualExchange
//
//  Created by Nilay Shah on 23/04/1938 Saka.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "PaymentCell.h"

@implementation PaymentCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)PromocodePress:(UIButton *)sender {
    
    if(sender.tag==1){
        
        if(self.didtapBlock)
            self.didtapBlock(sender);
        return;

    }
    
    if(!self.txtPromocode.text.isEmptyString){
        if(self.didtapBlock)
            self.didtapBlock(sender);
    }
}

@end
