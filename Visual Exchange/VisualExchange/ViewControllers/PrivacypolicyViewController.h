//
//  PrivacypolicyViewController.h
//  VisualExchange
//
//  Created by mac on 3/20/17.
//  Copyright © 2017 Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface PrivacypolicyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *objwebview;
@property(strong,nonatomic)NSString *privacystr;
@end
