//
//  ProductVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 21/04/1938 SAKA.
//  Copyright © 1938 SAKA Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLHelper.h"
#import "ProductCell.h"
#import "ProductDetailsVC.h"

@interface ProductVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) NSDictionary *category;
@property (nonatomic) NSDictionary *moreProduct;
@end
