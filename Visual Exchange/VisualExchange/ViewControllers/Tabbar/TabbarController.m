//
//  TabbarController.m
//  VisualExchange
//
//  Created by Minhaz on 6/24/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "TabbarController.h"

@interface TabbarController () <UITabBarDelegate,UITabBarControllerDelegate>
{
    
}
@end
@implementation TabbarController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor specSheetGreen]];
    
    self.delegate = self;
}

#pragma mark - UITabBarDelegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
}

#pragma mark - UITabBarControllerDelegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    
}
@end
