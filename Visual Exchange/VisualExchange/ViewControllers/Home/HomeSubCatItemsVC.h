//
//  HomeSubCatItemsVC.h
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "BaseVC.h"
#import "HomeProductDetailDelegate.h"
#import "PrefixHeader.pch"

@interface HomeSubCatItemsVC : BaseVC

@property (nonatomic) NSDictionary *category;
@property (nonatomic) NSDictionary *moreproduct;

@property (nonatomic) id <HomeProductDetailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *alertlbl;

// explicit call when manual reset (if controller is already rendered)
- (void)reloadItemList;

@end
