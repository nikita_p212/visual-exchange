//
//  HomeSubCategoryCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/25/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "HomeSubCategoryCell.h"

@implementation HomeSubCategoryCell
- (void)awakeFromNib {
    // Initialization code
    self.imageViewSubCategory.clipsToBounds = true;
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end
