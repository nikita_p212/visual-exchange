//
//  HomeProductDetailDelegate.h
//  VisualExchange
//
//  Created by Minhaz on 7/3/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HomeProductDetailDelegate <NSObject>

- (void)didSelectProduct:(NSDictionary *)item;
- (void)didSelectCatagory:(NSDictionary *)item;

@end
