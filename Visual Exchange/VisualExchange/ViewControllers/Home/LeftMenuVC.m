//
//  LeftMenuVC.m
//  VisualExchange
//
//  Created by Minhaz on 6/20/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "LeftMenuVC.h"
#import "LeftMenuTableCell.h"
#import "LeftMenuPersonInfoCell.h"
#import "SelleremailViewController.h"

NSString *cellIDPersonInfo      = @"cellIDPersonInfo";
NSString *cellIDSearch          = @"cellIDSearch";
NSString *cellIDMyProfile       = @"cellIDMyProfile";
NSString *cellIDChangePassword  = @"cellIDChangePassword";
NSString *cellIDMyCart          = @"cellIDMyCart";
NSString *cellIDOrderHistory    = @"cellIDOrderHistory";
NSString *cellIDSellOnVE        = @"cellIDSellOnVE";
NSString *cellIDWallet          = @"cellIDWallet";
NSString *cellIDLogout          = @"cellIDLogout";
NSString *cellIDprivacy         = @"cellIDPrivacy";
NSString *cellIDSHIPMENT        = @"cellIDShipment";
NSString *cellIDreturn          = @"cellIDReturn";

////

@interface LeftMenuVC () <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray *arrMenuItems;
    
    __weak IBOutlet UITableView *tblView;
}
@end

#define kKeyTitle       @"kKeyTitle"
#define kKeyImage       @"kKeyImage"
#define kKeyIdentifier  @"kKeyIdentifier"

@implementation LeftMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // initialize variables
    arrMenuItems = [NSMutableArray array];
    
    // default selected index
    
    
    // setup items
    [self setupItems];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadTableData];
}

#pragma mark - Setup row items
- (void)setupItems
{
    
    if(kUserDefults_(ResultUser))
    {
        [self addTitle:@"" imageName:@"" identifier:cellIDPersonInfo];
        [self addTitle:@"Search" imageName:@"Search.png" identifier:cellIDSearch];
        [self addTitle:kUserDefults_(ResultUser)?@"My Cart":@"Cart" imageName:@"My-Cart.png" identifier:cellIDMyCart];
        [self addTitle:@"Order History" imageName:@"Order-History.png" identifier:cellIDOrderHistory];
        [self addTitle:@"My Profile" imageName:@"My-Profile.png" identifier:cellIDMyProfile];
        [self addTitle:@"Change Password" imageName:@"change_password.png" identifier:cellIDChangePassword];
        
        [self addTitle:@"Sell on VE" imageName:@"Sell-on-VE.png" identifier:cellIDSellOnVE];
        
        
        NSString *type = kUserDefults_(ResultUser)[@"cust_type"];
        if([type isEqualToString:@"0"] )
        {
            
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
            [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"get_customer_type" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
             {
                 mailstr =  [NSString stringWithFormat:@"%@",[JSON objectForKey:@"cust_type"]];
                 
                 if([mailstr isEqualToString:@"1"])
                 {
                      [self addTitle:@"Wallet" imageName:@"Wallet.png" identifier:cellIDWallet];
                     
                 }
                 else{
                     
                 }
             }];
             

        }
        else
        {
          [self addTitle:@"Wallet" imageName:@"Wallet.png" identifier:cellIDWallet];
        }
        [self addTitle:@"Privacy Policy" imageName:@"prv" identifier:cellIDprivacy];
        [self addTitle:@"Return Policy" imageName:@"ret" identifier:cellIDreturn];
        [self addTitle:@"Shipping Policy" imageName:@"shipping" identifier:cellIDSHIPMENT];

        [self addTitle:@"Logout" imageName:@"Logout.png" identifier:cellIDLogout];
    }else{
        
        [self addTitle:@"" imageName:@"" identifier:cellIDPersonInfo];
        [self addTitle:@"Search" imageName:@"Search.png" identifier:cellIDSearch];
        [self addTitle:@"Cart" imageName:@"My-Cart.png" identifier:cellIDMyCart];
        [self addTitle:@"Sell on VE" imageName:@"Sell-on-VE.png" identifier:cellIDSellOnVE];
        [self addTitle:@"Privacy Policy" imageName:@"prv" identifier:cellIDprivacy];
        [self addTitle:@"Return Policy" imageName:@"ret" identifier:cellIDreturn];
        [self addTitle:@"Shipping Policy" imageName:@"shipping" identifier:cellIDSHIPMENT];
        [self addTitle:@"Login" imageName:@"Logout.png" identifier:cellIDLogout];
        
        
    }
   
    
}

- (void)addTitle:(NSString *)title imageName:(NSString *)imageName identifier:(NSString *)identifier
{
    NSDictionary *dict = @{
                           kKeyTitle : title,
                           kKeyImage : imageName,
                           kKeyIdentifier : identifier,
                           };
    [arrMenuItems addObject:dict];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Reload table data
- (void)reloadTableData
{
    tblView.delegate = self;
    tblView.dataSource = self;
    [tblView reloadData];
}

#pragma mark - Actions
- (IBAction)onBackBtClicked:(id)sender {
    
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = arrMenuItems[indexPath.row];
    if ([dict[kKeyIdentifier] isEqualToString:cellIDPersonInfo]) {
        LeftMenuPersonInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuPersonInfoCell"];
        return cell.contentView.frame.size.height;
    }
    
    return IS_IPAD?65:DLAutomatic(38);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMenuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSDictionary *dict = arrMenuItems[indexPath.row];
    NSString *keyIden = dict[kKeyIdentifier];
    if ([keyIden isEqualToString:cellIDPersonInfo]) {
        static NSString *cellIden = @"LeftMenuPersonInfoCell";
        LeftMenuPersonInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIden];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if(kUserDefults_( ResultUser)){
            cell.lblName.text = kUserDefults_(ResultUser)[@"name"];
            cell.lblEmail.text = kUserDefults_(ResultUser)[@"email"];
            NSLog(@"%@",cell.lblName.font.fontName);
            [[DLHelper shareinstance]DLImageStoreWithPlaceholderImage:[UIImage imageNamed:@"Default_640_1136"] imageview:cell.imageViewPhoto urlString:kUserDefults_(ResultUser)[@"image"]  Complete:^(UIImage *image) {
                
            }];
        }
        cell.imageViewPhoto.userInteractionEnabled  =true;
        
        UITapGestureRecognizer *tapGesture = [UITapGestureRecognizer new];
        [tapGesture addTarget:self action:@selector(profileTap:)];
        [cell.imageViewPhoto addGestureRecognizer:tapGesture];
        
        
        
        return cell;
    } else {
        static NSString *cellIden = @"LeftMenuTableCell";
        LeftMenuTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIden];
        cell.textLabel.font = [UIFont fontWithName:OpenSenceSB size:IS_IPAD?16: DLAutomatic(13)];
        cell.textLabel.textColor = [UIColor colorWithHexString:@"3b3b3b"];
        
        cell.textLabel.text = dict[kKeyTitle];
        cell.imageView.image = [UIImage imageNamed:dict[kKeyImage]];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.backgroundColor = [UIColor clearColor];
        
        if ([keyIden isEqualToString:cellIDChangePassword]) {
            cell.vSeperator.hidden = NO;
        } else {
            cell.vSeperator.hidden = YES;
        }
        if( [DLHelper shareinstance]. selectedIndex  == indexPath.row){
            cell.backgroundColor = [UIColor specSheetGreen];
            cell.selected = true;
        }
        return cell;
    }
}

#pragma mark - UITableViewDelegate

-(void)SelectedindexSetting:(NSNumber *)number{
    
    [DLHelper shareinstance]. selectedIndex  = [number integerValue];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = arrMenuItems[indexPath.row];
    NSString *keyIden = dict[kKeyIdentifier];
    
    // logout
    
    
    
    if (![keyIden isEqualToString:cellIDPersonInfo]) {
        
        //  LeftMenuTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        // cell.backgroundColor = [UIColor specSheetGreen];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:[DLHelper shareinstance]. selectedIndex inSection:0 ];
        LeftMenuTableCell *cell =  [tableView cellForRowAtIndexPath:indexpath];
        cell.backgroundColor = [UIColor clearColor];
        [self performSelector:@selector(SelectedindexSetting:) withObject:[NSNumber numberWithInteger:indexPath.row]afterDelay:0.1];
        
    }
    
    if ([keyIden isEqualToString:cellIDLogout]) {
        [self openLogoutConfirmationDialogue];
        return;
    }
    
    // close drawer
    [[AppLauncher sharedInstance] closeDrawer];
    
    
    if ([keyIden isEqualToString:@"cellIDSearch"]) {
        
        [[AppLauncher sharedInstance]Searchpress];
        
        return;
    }
    
    
       if (!kUserDefults_(ResultUser))
       {
        
//           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You need to Login/Sign up frist" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK" ,nil];
//           
//           [alert show];
//           
       // currently comment this code/////
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MyCartVC"];
//                    [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
//
//        
//        UINavigationController *navi = [DLHelper AppDelegate].tabbarController.selectedViewController;
//        if ([navi.topViewController isKindOfClass:[LoginVC class]]){
//            return;
//        }
//           
           
           // uncomment it whenerver not required////

//        [[DLHelper shareinstance]DLAlert:^(NSInteger DLindex) {
//            if(DLindex==0)
//                return;
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//            [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
//        } title:nil message:@"You need to Login/Sign up frist" canceltile:@"CANCEL" othertitle:@"OK", nil];
//        
       // return;
    }
    
    // update selected cell background color
    if (![keyIden isEqualToString:cellIDPersonInfo]) {
        
        //  LeftMenuTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        // cell.backgroundColor = [UIColor specSheetGreen];
        
        [DLHelper shareinstance]. selectedIndex  = indexPath.row;
    }
    
    // my profile
    if ([keyIden isEqualToString:cellIDMyProfile]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:3];
        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"ProfileEditVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
    }
    if ([keyIden isEqualToString:cellIDSellOnVE]) {
        if(!kUserDefults_(ResultUser))
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You need to Login/Sign up first" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK" ,nil];
           
            [alert show];
            
        }
        else{
            
           NSString *type = kUserDefults_(ResultUser)[@"cust_type"];
            if([type isEqualToString:@"0"] )
            {
                               NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
                [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"get_customer_type" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
                 {
                    mailstr =  [NSString stringWithFormat:@"%@",[JSON objectForKey:@"cust_type"]];
               
                     if([mailstr isEqualToString:@"1"])
                     {
                          [[AppLauncher sharedInstance]ShellonVR];
                     }
                     else{
                     NSString *statuscheck= [NSString stringWithFormat:@"%@",[JSON objectForKey:@"is_request_mail"] ];
                     if([statuscheck isEqualToString:@"1"])
                     {
                     
                     
                         NSUserDefaults *default5=[NSUserDefaults standardUserDefaults];
                         [default5 setObject:[JSON objectForKey:@"is_request_mail"] forKey:@"status"];
                         [default5 synchronize];
                         
                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                         UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
                         UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SelleremailViewController"];
                         
                         navigation.viewControllers = @[leftDrawer].mutableCopy;

                   
                     }
                       else  if([statuscheck isEqualToString:@"0"])
                         {
                             
                             
                             NSUserDefaults *default5=[NSUserDefaults standardUserDefaults];
                             [default5 setObject:[JSON objectForKey:@"is_request_mail"] forKey:@"status"];
                             [default5 synchronize];
                             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                             UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
                             UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"SelleremailViewController"];
                             
                             navigation.viewControllers = @[leftDrawer].mutableCopy;
                             
                             
                         }

                     }
                 }];
                

                
                
            }
            else if([type isEqualToString:@"1"] )
            {
                  [[AppLauncher sharedInstance]ShellonVR];
            }
            else
            {
               NSString *str=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"status"]];
                 
                if([str isEqualToString:@"1"])
                {
                     [[AppLauncher sharedInstance]ShellonVR];
                }
            
            }
        }
        
       
        
    }
    
    // change password
    if ([keyIden isEqualToString:cellIDChangePassword]) {
        [[AppLauncher sharedInstance] openChangePassword];
    }
    
    // my cart
    if ([keyIden isEqualToString:cellIDMyCart]) {
        [[AppLauncher sharedInstance] setTabbarSelectedIndex:1];
        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
        [navigation popToRootViewControllerAnimated:false];
    }
    
    if ([keyIden isEqualToString:cellIDOrderHistory]) {
        
        
        [[AppLauncher sharedInstance]orderPress];
        return;
    }
    if ([keyIden isEqualToString:cellIDWallet]) {
        
        
        
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:2];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
        UIViewController * leftDrawer = [storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
        navigation.viewControllers = @[leftDrawer].mutableCopy;
        
        
        
        return;
    }
    if ([keyIden isEqualToString:cellIDSHIPMENT])
    {
        
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:0];
        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
              UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PrivacypolicyViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PrivacypolicyViewController"];
        vc.privacystr=@"shipment";
        navigation.viewControllers = @[vc].mutableCopy;
        
        
        

        
    }
    if ([keyIden isEqualToString:cellIDreturn])
    {
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:0];
        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PrivacypolicyViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PrivacypolicyViewController"];
        vc.privacystr=@"return";
        navigation.viewControllers = @[vc].mutableCopy;
        
        
    }
    if ([keyIden isEqualToString:cellIDprivacy])
    {
        [[DLHelper AppDelegate].tabbarController setSelectedIndex:0];

        UINavigationController *navigation = [DLHelper AppDelegate].tabbarController.selectedViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PrivacypolicyViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PrivacypolicyViewController"];
        vc.privacystr=@"privacy";
        navigation.viewControllers = @[vc].mutableCopy;
        
        
    }
    
    
}

#pragma mark - Logout confirmation Dialogue
- (void)openLogoutConfirmationDialogue
{
    
    if(!kUserDefults_(ResultUser)){
        [[AppLauncher sharedInstance] closeDrawer];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        vc.hidesBottomBarWhenPushed = true;
        UINavigationController * navi = [DLHelper AppDelegate].tabbarController.selectedViewController;
        if(![navi.topViewController isKindOfClass:[LoginVC class]]){
            [[DLHelper AppDelegate].tabbarController.selectedViewController pushViewController:vc animated:YES];
            
        }
        
        return;
        
    }
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"No",@"Yes", nil];
    alert.tag = 100;
    [alert show];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        if (buttonIndex == 0) {//No
            // do nothing
        }
        else if(buttonIndex == 1) { //Yes
            
            
           
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];

            
            [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"logout.php" UrlBody:dict.mutableCopy Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
             {
                 NSLog(@"%@",JSON);
                 NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
                 [defs removeObjectForKey:ResultUser];
                 [defs removeObjectForKey:ISLOGIN];
                 [defs removeObjectForKey:@"cartary"];
                 [defs synchronize];
                 
                 if([[JSON objectForKey:@"status"]integerValue]==true)
                 {
                     [[DLHelper AppDelegate]isLoginUser];

                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[JSON objectForKey:@"message"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    
                     [alert show];
                 }
             }];

            
            
            
            
        }
    }
}
- (IBAction)profileTap:(id)sender {
    if(!kUserDefults_(ResultUser)){
        [[AppLauncher sharedInstance] closeDrawer];
        [[AppLauncher sharedInstance] setTabbarSelectedIndex:0];
        return;
    }
    
    [[AppLauncher sharedInstance] closeDrawer];
    [[AppLauncher sharedInstance] setTabbarSelectedIndex:3];
}

@end
