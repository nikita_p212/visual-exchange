//
//  SubCatItemCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/26/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "SubCatItemCell.h"

@implementation SubCatItemCell
- (void)awakeFromNib {
    // Initialization code
    self.imageViewItem.clipsToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
}
@end
