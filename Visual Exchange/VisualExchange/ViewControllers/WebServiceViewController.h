//
//  WebServiceViewController.h
//  Diet Calorie Counter
//
//  Created by User Name on 5/17/13.
//  Copyright (c) 2013 xoomsolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SystemConfiguration/SystemConfiguration.h>

#define Place_URL @"http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/json3.ws"
#define Geocode_URL @"http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/json.ws"

//#define Place_URL @"http://services.postcodeanywhere.co.uk/PostcodeAnywhere/interactive/Find/v1.10/json3.ws"

#define LatLong_URL @"http://services.postcodeanywhere.co.uk/Geocoding/UK/Geocode/v2.00/json3.ws"

@protocol WebServiceDelegate <NSObject>
@optional
- (void)webserviceConnection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)webserviceConnectionDidFinishLoading:(NSURLConnection *)connection successFullyGotData:(NSMutableData *)data currentReqMethod:(NSString *)strCurrentReqMethod;

- (void)webserviceconnection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
   totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite;

@end

@interface WebServiceViewController : UIViewController

+ (WebServiceViewController*) wsVC;

@property(nonatomic,retain)id <WebServiceDelegate> delegate;
@property(nonatomic,strong)NSMutableData *responseData;
@property(nonatomic,strong)NSString *strCurrentRequestMethod;



@property (nonatomic, strong) NSString *strURL;
@property (nonatomic, strong) NSString *jsonString;
@property (nonatomic, strong) NSString *strCallHttpMethod;
@property (nonatomic, strong) NSMutableData *activeDownload;
@property (nonatomic, strong) NSURLConnection *imageConnection;
@property (nonatomic, strong) NSString *strMethodName;

- (NSMutableDictionary *)sendRequestForMethod:(NSString *)strMethod withParameter:(NSMutableDictionary *)dictDetail;

- (NSMutableDictionary *)sendRequestForMethod:(NSString *)strMethod withParameter:(NSMutableDictionary *)dictDetail withImageArray:(NSMutableArray *)arrImages;

- (NSMutableDictionary *)sendRequestWithParameter:(NSMutableDictionary *)dictDetail;
- (NSMutableDictionary *)sendReqGoogleApiWith:(NSMutableDictionary *)dictDetail;
- (BOOL)connected;

+ (NSMutableDictionary *)simpleURLFetch:(NSString * )methodName param:(NSDictionary *)jsonDict;

-(void)PostData:(NSMutableDictionary *)dictDetail;
-(BOOL)isValidEmail:(NSString *)strEmail;
@end
