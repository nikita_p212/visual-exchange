//
//  ProfilePhotoCell.h
//  VisualExchange
//
//  Created by Minhaz on 6/25/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePhotoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btProfileImage;

@end
