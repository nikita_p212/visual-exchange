//
//  ProfileTextInputCell.m
//  VisualExchange
//
//  Created by Minhaz on 6/25/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "ProfileTextInputCell.h"

@implementation ProfileTextInputCell

- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame = _vSeperator.frame;
    frame.origin.y = _vSeperator.superview.frame.size.height - 0.5;
    frame.size.height = 0.5;
    _vSeperator.frame = frame;
}

@end
