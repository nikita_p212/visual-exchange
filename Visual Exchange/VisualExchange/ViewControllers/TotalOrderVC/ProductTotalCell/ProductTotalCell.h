//
//  ProductTotalCell.h
//  VisualExchange
//
//  Created by Nilay Shah on 05/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTotalCell : UITableViewCell<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgproduct;
@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
-(void)setDidtapBlock:(void(^)(UIButton *button))compition;
@property(strong,nonatomic)void(^didtapBlock)(UIButton *button);
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@end
