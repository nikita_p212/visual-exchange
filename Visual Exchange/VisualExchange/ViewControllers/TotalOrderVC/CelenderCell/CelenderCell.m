//
//  CelenderCell.m
//  VisualExchange
//
//  Created by Nilay Shah on 29/05/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "CelenderCell.h"

@implementation CelenderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if (IS_IPAD){
        
        for(UIView *btn in self.viewTop.subviews){
            if([btn isKindOfClass:[UIButton class]]){
            if (btn.tag == 1){
                
                CGRect rect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.height, btn.frame.size.height);
                btn.frame = rect;
                btn.layer.cornerRadius = btn.frame.size.width/2;
                btn.layer.masksToBounds = true;
               // self.lblStaticRevenue.center = CGPointMake(btn.center.x,self.lblStaticRevenue.center.y);
               // self.lblRevenue.frame.size = self.lblStaticRevenue.frame.size;
                
                btn.center = CGPointMake(self.lblRevenue.center.x,btn.center.y);
                
            }
            if (btn.tag == 2){
                CGRect rect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.height, btn.frame.size.height);
                btn.frame = rect;
                btn.layer.cornerRadius = btn.frame.size.width/2;
                btn.layer.masksToBounds = true;
                //                self.lblStaticTotalproduct.center = CGPointMake(btn.center.x,self.lblStaticTotalproduct.center.y);
                //                self.lblTotalProduct.center = CGPointMake(btn.center.x,self.lblTotalProduct.center.y);
                //  self.lblPaid.center =  CGPointMake(self.lblStaticPaid.center.x, self.lblPaid.center.y);
                btn.center = CGPointMake(self.lblPaid.center.x,btn.center.y);
            }
            if (btn.tag == 3){
                CGRect rect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.height, btn.frame.size.height);
                btn.frame = rect;
                btn.layer.cornerRadius = btn.frame.size.width/2;
                btn.layer.masksToBounds = true;
                //                self.lblStaticTotalproduct.center = CGPointMake(btn.center.x,self.lblStaticTotalproduct.center.y);
                //                self.lblTotalProduct.center = CGPointMake(btn.center.x,self.lblTotalProduct.center.y);
                //  self.lblBalence.center = CGPointMake(self.lblstaticbalance.center.x, self.lblBalence.center.y) ;
                btn.center = CGPointMake(self.lblBalence.center.x,btn.center.y);
                
            }
            }
        }
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)OrderPress:(UIButton *)sender {
  //  ordertag = sender.tag;
    for(UIView *btn in sender.superview.subviews){
        if(btn.tag > 0){
            
            if([btn isKindOfClass:[UIButton class]]){
                
                UIButton *button = (UIButton *)btn;
                if(sender.tag==btn.tag){
                    button.selected = true;
                    
                }else{
                    button.selected = false;
                }
            }if([btn isKindOfClass:[UILabel class]]){
                
                UILabel *button = (UILabel *)btn;
                if(sender.tag==btn.tag){
                    button.textColor = [UIColor colorWithRed:153/255.f green:193/255.f blue:58/255.f alpha:1.0];
                    
                }else{
                    button.textColor = [UIColor lightGrayColor];
                }
            }
        }
    }

    [self.delegate OrderPress:sender];
   //
}
- (IBAction)DatePress:(UIButton *)sender {
    [self.delegate DatePress:sender];

}
- (IBAction)DonePress:(id)sender{
     [self.delegate DonePress:sender];
}
@end
