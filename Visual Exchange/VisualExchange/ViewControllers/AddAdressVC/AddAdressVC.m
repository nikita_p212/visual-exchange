//
//  AddAdressVC.m
//  VisualExchange
//
//  Created by Nilay Shah on 24/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "AddAdressVC.h"
#import "CartSecondViewController.h"
@interface AddAdressVC (){
    NSMutableArray *arrStates;
   
}
@property (weak, nonatomic) IBOutlet UILabel *lbltitle;

@end

@implementation AddAdressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnUpdate.layer.cornerRadius =_btnUpdate.frame.size.height/2;
    _btnUpdate.layer.masksToBounds=YES;

    
  self.lbltitle.text = @"Add Address";
    
    if(!self.isNew){
        self.lbltitle.text = @"Edit Address";
        NSMutableDictionary *dict = [self.result mutableCopy];;
    self.txtName.text = dict[@"name"];
    self.txtAddress.text = dict[@"address"];
    self.txtCity.text = dict[@"city"];
    self.txtState.text = dict[@"state"];
    self.txtCountry.text= dict[@"country"] ;
    self.txtZipcode.text = dict[@"zipcode"];
    self.txtMobile.text = dict[@"phone"];
    }
    self.txtEmail.text = kUserDefults_(ResultUser)[@"email"];
       
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self AutomaticScrollview];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Get uer object
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == self.txtState){
        [self fetchStates];
        [textField resignFirstResponder];
        
    }
    return true;
}


#pragma mark - Actions
- (IBAction)onMenuButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)onUpdateBtClicked:(id)sender {
    [self.view endEditing:YES];
    
    
    if (self.txtName.text.isEmptyString) {
        [self showErrorWithMessage:dlkName];
    }
    
    else if (!self.txtEmail.text.validateEmailWithString) {
        [self showErrorWithMessage:dlkEmail];
    }
    else if (self.txtAddress.text.isEmptyString) {
        [self showErrorWithMessage:dlkAddress];
    }
    else if (self.txtCity.text.isEmptyString) {
        [self showErrorWithMessage:dlkCity];
    }
    else if (self.txtState.text.isEmptyString) {
        [self showErrorWithMessage:dlkState];
    }
    else if (self.txtCountry.text.isEmptyString) {
        [self showErrorWithMessage:dlkCountry];
    }
    else if (self.txtZipcode.text.isEmptyString) {
        [self showErrorWithMessage:dlkZipcode];
    }
    
    else if (!self.txtMobile.text.validatePhoneNumber) {
        [self showErrorWithMessage:dlkMobile];
    }
   
    else{
        NSString * stateid = nil;
        for(int i=0; i<arrStates.count; i++){
            if([self.txtState.text isEqualToString:arrStates[i][@"state"]]){
                stateid = arrStates[i][@"state_code"];
                break;break;
            }
        }
        
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"customer_id"] = kUserDefults_(ResultUser)[@"customer_id"];
        dict[@"name"] = self.txtName.text;
        dict[@"email"] = self.txtEmail.text;
        dict[@"address"] = self.txtAddress.text;
        dict[@"city"] = self.txtCity.text;
        dict[@"state"] = self.txtState.text;
        dict[@"country"] = self.txtCountry.text;
        dict[@"zipcode"] =self.txtZipcode.text;
        dict[@"phone"] = self.txtMobile.text;
        dict[@"customer_address_id"] = self.result[@"address_id"];
        
        if(self.result){
            
            
            [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"editCustomerAddress.php" UrlBody:dict Complete:^(NSMutableDictionary *JSON, NSInteger statuscode)
            {
                CartSecondViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CartSecondViewController"];
               // vc.cartpassary = arrayCart;
                [self.navigationController pushViewController:vc animated:YES];

                
               // [self.navigationController popViewControllerAnimated:true];
            }];
            return;

        }
        
        [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"addCustomerAddress.php" UrlBody:dict Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
            CartSecondViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CartSecondViewController"];
           // vc.cartpassary = arrayCart;
            [self.navigationController pushViewController:vc animated:YES];

    
           // [self.navigationController popViewControllerAnimated:true];
        }];
        
    }
}


#pragma mark - Fetch states
- (void)fetchStates
{
    [self.view endEditing:YES];
    
    [[DLHelper shareinstance]DLSERVICE:self.view Indicater:true url:@"states.php" UrlBody:nil Complete:^(NSMutableDictionary *JSON, NSInteger statuscode) {
        arrStates = [JSON[@"states"]mutableCopy];
        PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
        pickerViewController.pickerType = CustomPickerType;
        pickerViewController.dataSourceForCustomPickerType = [arrStates valueForKey:@"state"];
        [pickerViewController setInitialItemAtIndex:0];
        pickerViewController.delegate = self;
        [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
        [DLHelper AppDelegate].tabbarController.tabBar.hidden = true;
        
        
    }];
    
}
- (void)didSelectItemAtIndex:(NSUInteger)index{
    self.txtState.text = arrStates[index][@"state"];
}


#pragma mark - UITextFieldDelegate-
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}




-(void)AutomaticScrollview{
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollview.subviews)
        contentRect = CGRectUnion(contentRect, view.frame);
    self.scrollview.contentSize = CGSizeMake(0, contentRect.size.height+100);
}


@end
