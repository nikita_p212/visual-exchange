//
//  AddProductVC.h
//  VisualExchange
//
//  Created by Nilay Shah on 28/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddProductVC : BaseVC<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
{
    NSMutableArray *idary;
}
@property (weak, nonatomic) IBOutlet UIView *datepikcerview;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
- (IBAction)cancelbtnpress:(id)sender;
- (IBAction)donebtnpress:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtlegth;
- (IBAction)leadimebtnpress:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scollMain;
@property (weak, nonatomic) IBOutlet UITextField *txtProductname;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtWight;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;
@property (weak, nonatomic) IBOutlet UIView *viewAlltextfield;
@property (weak, nonatomic) IBOutlet UITextField *txtSKU;
@property (weak, nonatomic) IBOutlet UITextField *txtleadtime;
@property (weak, nonatomic) IBOutlet UITextField *txtwidth;
@property(weak,nonatomic)NSMutableDictionary *result;
@property (weak, nonatomic) IBOutlet UITextField *txtVideoLink;
- (IBAction)catbtnpress:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtstocklimit;
@property (weak, nonatomic) IBOutlet UITextField *txtheight;
@property(strong,nonatomic)NSMutableArray *passcateary;
@end
