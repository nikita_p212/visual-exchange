   //
//  DLHelper.m
//  UFeedback
//
//  Created by Moweb_10 on 20/04/16.
//  Copyright © 2016 Moweb. All rights reserved.
//

#import "DLHelper.h"

@implementation DLHelper
static DLHelper *shareinstance;
+(DLHelper *)shareinstance
{
    if(shareinstance==nil){
        
        shareinstance=[[self alloc]init];
        [DLHelper shareinstance]. selectedIndex = 1000;
    }
    return shareinstance;
}

#pragma - DIRECT USER HELPER -

+(NSString *)UDID{
    
    return  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+(NSString *)DEVICETOKEN{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *token=  [NSString stringWithFormat:@"%@",[ defaults valueForKey:@"DEVICETOKEN"]];
    token = [[token description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    return  token;
}

+(NSData *)DEVICETOKEN_DATA{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return   [ defaults valueForKey:@"DEVICETOKEN"];
}

+(NSString *)ACCESSTOKEN{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return  [NSString stringWithFormat:@"%@",[ defaults valueForKey:@"ACCESSTOKEN"]];
}

+(NSString *)USERID{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return  [NSString stringWithFormat:@"%@",[ defaults valueForKey:@"USERID"]];
}
+(NSInteger)LOGIN_TYPE{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return  [[NSString stringWithFormat:@"%@",[ defaults valueForKey:@"LOGIN_TYPE"]]integerValue];
}
+(NSString *)USEREMAIL{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return  [NSString stringWithFormat:@"%@",[ defaults valueForKey:@"USEREMAIL"]];
}

+(NSString *)PROFILE_PIC{
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    return  [NSString stringWithFormat:@"%@",[ defaults valueForKey:@"PROFILE_PIC"]];
}


-(void)DLSERVICE:(UIView *)view Indicater:(BOOL)indicater url:(NSString *)apiUrl UrlBody:(NSMutableDictionary *)apiBody Complete:(void(^)(NSMutableDictionary *JSON,NSInteger statuscode))completed{
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {

        
        UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"No Internet Connection"message:dlkNoInterNet
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [myAlert show];
        return;
    }
    
    if(indicater){
        
        [MBProgressHUD showHUDAddedTo:view animated:YES].labelText=@"Loading...";
    }
    if(apiBody==nil){
        apiBody = [NSMutableDictionary new];
    }
    
   
    
    NSMutableString *apiParameter = [NSMutableString string];
    for (NSString* key in [apiBody allKeys]){
        if ([apiParameter length]>0)
            [apiParameter appendString:@"&"];
        if([apiBody [key] isKindOfClass:[NSString class]]){
              apiBody[key] = [[apiBody objectForKey:key] stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        }
      
        [apiParameter appendFormat:@"%@=%@", key, [apiBody objectForKey:key]];
    }
    //    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:apiBody options:0 error:nil];
    //    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    NSURL *fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,apiUrl]];
    
    if ([apiUrl isEqualToString:@"totalorders"]||[apiUrl isEqualToString:@"wallet"] || [apiUrl isEqualToString:@"order_history"]){
        fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL_NEW,apiUrl]];
        
    }
    if ([apiUrl isEqualToString:@"image_delete"] ||[apiUrl isEqualToString:@"check_coupon"]||[apiUrl isEqualToString:@"check_address"]||[apiUrl isEqualToString:@"cart_delete"]||[apiUrl isEqualToString:@"cart_update"]||[apiUrl isEqualToString:@"add_order"]||[apiUrl isEqualToString:@"update_order"]){
        fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://www.thevisualexchange.com/visualweb/api/",apiUrl]];
        
    }
    if ([apiUrl isEqualToString:@"change_customer_type"] ||[apiUrl isEqualToString: @"get_customer_type"])
         {
               fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://www.thevisualexchange.com/visualweb/api/",apiUrl]];
         }
    if ([apiUrl isEqualToString:@"cartlist"] ||[apiUrl isEqualToString:@"customeraddress"] ||[apiUrl isEqualToString:@"checkout"] )
    {
         fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://www.thevisualexchange.com/visualweb/api/get/",apiUrl]];
    }
    if([apiUrl isEqualToString:@"logout.php"])
    {
         fullUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://thevisualexchange.com/visual/api/",apiUrl]];
        
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:fullUrl];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: [apiParameter dataUsingEncoding: NSUTF8StringEncoding]];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSHTTPURLResponse *response;
        NSError *error;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if(view){
                [MBProgressHUD hideAllHUDsForView:view animated:YES];
            }
            if(data != nil)
            {
                
                
                NSString *content=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"fullURL:- %@\n\n",fullUrl);
                NSLog(@"fullURL:- %@\n\n",apiBody);
                NSLog(@"fullURL:- %@\n\n",content);
                if(content.length != 0)
                {
                    NSError *e = nil;
                    NSMutableDictionary *JSON = [NSJSONSerialization  JSONObjectWithData: data
                                                                                 options: NSJSONReadingMutableContainers error: &e];
                    [JSON setValue:[NSString stringWithFormat:@"%ld",(long)response.statusCode] forKey:@"Status"];
                    if(JSON.count==0)
                    {
                         completed(JSON,1);
                    }
                  else  if ([apiUrl isEqualToString:@"cartlist"] )
                    {
                         completed(JSON,1);
                    }
                 else   if([JSON[@"Status"]integerValue]==1){
                        completed(JSON,1);
                    }
                   else if([JSON[@"Status"]integerValue]==200){
                        completed(JSON,200);
                    }
                   else if([JSON[@"status"]integerValue]==0){
                        completed(JSON,0);
                    }
                    else{
                        
                    
//                        NSString *errorMessage = JSON[@"message"]==nil ?JSON[@"messages"]:JSON[@"message"];
//                                               UIAlertView *alertView = [[UIAlertView alloc]
//                                                  initWithTitle:@"VisualExchange"
//                                                  message:errorMessage
//                                                  delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//                        [alertView show];
//                        
                    }
                    
                }
                
            }else{
//                UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"VisualExchange"message:dlkServer_Off
//                                                                delegate:self
//                                                       cancelButtonTitle:@"Ok"
//                                                       otherButtonTitles:nil];
//                [myAlert show];

            }
            
        });
    });
    
    
    
    
    
}


-(void)DLImageStore:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed{
    
    UIActivityIndicatorView *progress= [[UIActivityIndicatorView alloc] init];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    if(imageview){
        progress.center=CGPointMake(imageview.frame.size.width/2, imageview.frame.size.height/2);
        [progress hidesWhenStopped];
        [imageview addSubview:progress];
        progress.color=[UIColor grayColor];
        imageview.image = nil;
    }
    NSString *str=[url stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *file=[dirPaths stringByAppendingPathComponent:str];
    UIImage *image=[UIImage imageWithContentsOfFile:file];
    if(image)
    {
        imageview.image = image;
        completed(image);
    }
    else{
        if(url){
            [progress startAnimating];
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
                    NSString *file=[dirPaths stringByAppendingPathComponent:str];
                    [data writeToFile:file atomically:YES];
                    UIImage *image=[UIImage imageWithContentsOfFile:file];
                    completed(image);
                    if(imageview){
                        imageview.image = image;
                        [progress stopAnimating];
                        [progress removeFromSuperview];
                    }
                });
            });
        }
        
    }
    
    
    
}
-(void)DLImageStoreWithPlaceholderImage:(UIImage *)imageplacehoder imageview:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed{
    
    imageview.image=imageplacehoder;
    NSString *str=[url stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *file=[dirPaths stringByAppendingPathComponent:str];
    UIImage *image=[UIImage imageWithContentsOfFile:file];
    if(image)
    {
        imageview.image = image;
        completed(image);
    }
    else{
        if(url){
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
                    NSString *file=[dirPaths stringByAppendingPathComponent:str];
                    [data writeToFile:file atomically:YES];
                    UIImage *image=[UIImage imageWithContentsOfFile:file];
                    
                    completed(image);
                    if(imageview){
                        imageview.image = image;
                    }
                    if(image == nil){
                        imageview.image=imageplacehoder;
                    }
                });
            });
        }
        
    }
    
    
    
}

-(void)DLImageStoreWithPlaceholderImage:(UIImage *)imageplacehoder Bureimageview:(UIImageView *)bureimageview imageview:(UIImageView *)imageview urlString:(NSString *)url Complete:(void(^)(UIImage *image))completed{
   url =  [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    imageview.image=imageplacehoder;
    bureimageview.image = [self blurredImageWithImage:imageplacehoder];
    NSString *str=[url stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *file=[dirPaths stringByAppendingPathComponent:str];
    UIImage *image=[UIImage imageWithContentsOfFile:file];
    if(image)
    {
        imageview.image = image;
        bureimageview.image =  [self blurredImageWithImage:image];
        completed(image);
    }
    else{
        if(url){
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    NSString *  dirPaths = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
                    NSString *file=[dirPaths stringByAppendingPathComponent:str];
                    [data writeToFile:file atomically:YES];
                    UIImage *image=[UIImage imageWithContentsOfFile:file];
                    completed(image);
                    if(imageview){
                        imageview.image = image;
                        bureimageview.image =  [self blurredImageWithImage:image];
                    }
                });
            });
        }
        
    }
    
    
    
}


-(void)pedingAll:(UIView *)view
{
    for(UIView *v in view.subviews)
    {
        if([v isKindOfClass:[UIScrollView class]])
        {
            UIScrollView *scroll=(UIScrollView *)v;
            
            
            for(UIScrollView *v in scroll.subviews)
            {
                if([v isKindOfClass:[UITextField class]]){
                    
                    UITextField *textField = (UITextField *)v;
                    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
                    textField.leftView = paddingView;
                    textField.leftViewMode = UITextFieldViewModeAlways;
                }
            }
        }
        if([v isKindOfClass:[UITextField class]]){
            
            UITextField *textField = (UITextField *)v;
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 20)];
            textField.leftView = paddingView;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }}
    
}
-(void)TrimmingSingTextField
{
    _textfild.text = [_textfild.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
//  This App All TextField in ScrollView  bui u can change for(UIView *v in self.view.subvies)

-(void)TrimingAllTextfiled:(UIView *)view
{
    
    for(UIView *v in view.subviews)
    {
        if([v isKindOfClass:[UIScrollView class]])
        {
            UIScrollView *scroll=(UIScrollView *)v;
            
            
            for(UIScrollView *v in scroll.subviews)
            {
                if([v isKindOfClass:[UITextField class]]){
                    
                    UITextField *textField = (UITextField *)v;
                    
                    if(textField !=_textfild)
                    {
                        textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                    }}
                
            }
        }
        if([v isKindOfClass:[UITextField class]]){
            
            UITextField *textField = (UITextField *)v;
            
            if(textField !=_textfild)
            {
                textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
            }}
        
    }
}
-(NSString *)Trimmingstring:(NSString *)obj
{
    return [obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(void)KeyboardHide:(UIView *)view
{
    for(UIView *v in view.subviews)
    {
        if([v isKindOfClass:[UIScrollView class]])
        {
            UIScrollView *scroll=(UIScrollView *)v;
            
            
            for(UIScrollView *v in scroll.subviews)
            {
                if([v isKindOfClass:[UITextField class]]){
                    
                    UITextField *textField = (UITextField *)v;
                    [textField resignFirstResponder];
                }
            }
        }
        if([v isKindOfClass:[UITextField class]]){
            
            UITextField *textField = (UITextField *)v;
            [textField resignFirstResponder];
        }}
}
-(void)ClearTextfield:(UIView *)view
{
    for(UIView *v in view.subviews)
    {
        if([v isKindOfClass:[UIScrollView class]])
        {
            UIScrollView *scroll=(UIScrollView *)v;
            
            
            for(UIScrollView *v in scroll.subviews)
            {
                if([v isKindOfClass:[UITextField class]]){
                    
                    UITextField *textField = (UITextField *)v;
                    textField.text=@"";                }
            }
        }
        if([v isKindOfClass:[UITextField class]]){
            
            UITextField *textField = (UITextField *)v;
            textField.text=@"";
        }}
}
#pragma mark - UiactionSheet delegate -

#pragma mark - YHActionSheet Delegates -

- (CGFloat)getLabelHeight:(UILabel*)label
{
    if(label==nil || label.text.length==0){
        return 0;
    }
    CGSize constraint = CGSizeMake(label.frame.size.width, 20000.0f);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height ;
}

-(void)DLSERVICE_IMAGE:(UIView *)view Indicater:(BOOL)indicater url:(NSString *)apiUrl UrlBody:(NSMutableDictionary *)apiBody Complete:(void(^)(NSMutableDictionary *JSON,NSInteger statuscode))completed{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {

        
        UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"No Internet Connection"message:dlkNoInterNet
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [myAlert show];
        return;
    }
    

    if(indicater){
        [MBProgressHUD showHUDAddedTo:view animated:YES].labelText=@"Loading...";
    }
    if(apiBody==nil){
        apiBody = [NSMutableDictionary new];
    }
    
//    [apiBody setObject:kDEVICETOKEN forKey:@"deviceToken"];
//    [apiBody setObject:@"ios" forKey:@"deviceType"];
//    [apiBody setValue:@"q1w2e2rt3y5u6i8iug4h58f1f" forKey:@"apiKey"];
//    [apiBody setValue:[DLHelper shareinstance].lattitude?[DLHelper shareinstance].lattitude :@"00.00" forKey:@"latitude"];
//    [apiBody setValue:[DLHelper shareinstance].longitude?[DLHelper shareinstance].longitude :@"00.00" forKey:@"longitude"];
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,apiUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    NSMutableData *body = [NSMutableData data];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    
    
    for( NSString *str in  apiBody.allKeys){
        
        if([apiBody[str] isKindOfClass:[NSString class]]){
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",str] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[apiBody[str] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else if([apiBody[str] isKindOfClass:[UIImage class]]){
            
            UIImage *image  = apiBody[str];
            NSData *imageData = UIImageJPEGRepresentation(image, .20);
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"a.jpg\"\r\n",str] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else if([apiBody[str] isKindOfClass:[NSMutableData class]]){
            
            [body appendData:apiBody[str]];

        }
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSHTTPURLResponse *response;
        NSError *error;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if(view && indicater){
                [MBProgressHUD hideAllHUDsForView:view animated:YES];
            }
            if(data != nil)
            {
                
                
                NSString *content=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"fullURL:- %@\n\n",apiUrl);
                NSLog(@"fullURL:- %@\n\n",apiBody);
                NSLog(@"fullURL:- %@\n\n",content);
                if(content.length != 0)
                {
                    NSError *e = nil;
                    NSMutableDictionary *JSON = [NSJSONSerialization  JSONObjectWithData: data
                                                                                 options: NSJSONReadingMutableContainers error: &e];
                    [JSON setValue:[NSString stringWithFormat:@"%ld",(long)response.statusCode] forKey:@"Status"];
                    if([JSON[@"status"]integerValue]==1){
                        completed(JSON,1);
                    }
                    else{
                       
                        
                      
//                            UIAlertView *alertView = [[UIAlertView alloc]
//                                                      initWithTitle:@"VisualExchange"
//                                                      message:JSON[@"message"]
//                                                      delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                            [alertView show];
                            
                        

                        
                    }
                    
                }
                
            }else{
//                UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"VisualExchange"message:dlkServer_Off
//                                                                delegate:self
//                                                       cancelButtonTitle:@"Ok"
//                                                       otherButtonTitles:nil];
//                [myAlert show];
            }
            
        });
    });
    
    
}

-(void)DLAlert:(DLAlertView)alertView title:(NSString *)title message:(NSString *)message canceltile:(NSString *)canceltile othertitle:(NSString *)othertitle, ...NS_REQUIRES_NIL_TERMINATION {
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:canceltile otherButtonTitles: othertitle, nil];
    NSString *other = nil;
    va_list args;
    if (othertitle) {
        va_start(args, othertitle);
        while ((other = va_arg(args, NSString*))) {
            [alert addButtonWithTitle:other];
        }
        va_end(args);
    }
        [alert show];
    self.alertView = alertView;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(self.alertView){
        self.alertView(buttonIndex);
    }
}
+(AppDelegate *)AppDelegate{
    
return  (AppDelegate *)[UIApplication sharedApplication].delegate;

}

@end
