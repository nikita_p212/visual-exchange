//
//  AppLauncher.h
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeSubCatItemsVC.h"

@class MMDrawerController;

@interface AppLauncher : NSObject

// shared instance
+ (AppLauncher *)sharedInstance;

// get window with window
- (UIWindow *)windowWithWindow:(UIWindow *)window;

// set login/logout : by giving bool value
- (void)login:(BOOL)value;

// drawer
- (MMDrawerController *)drawerController;

// drawer actions
- (void)openLeftDrawer;
- (void)closeDrawer;

// set tabbar selected index
- (void)setTabbarSelectedIndex:(NSInteger)tabIndex;

// open change password screen
- (void)openChangePassword;

- (void)Searchpress;
-(void)orderPress;
-(void)ShellonVR;
-(void)privacy;

@property (nonatomic) HomeSubCatItemsVC *topItemVC;

@end
