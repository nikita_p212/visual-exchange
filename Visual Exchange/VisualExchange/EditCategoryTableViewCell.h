//
//  EditCategoryTableViewCell.h
//  VisualExchange
//
//  Created by mac on 12/22/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditCategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *catbtn;
@property (weak, nonatomic) IBOutlet UILabel *catlbl;
@property (weak, nonatomic) IBOutlet UIImageView *chekimg;

@end
