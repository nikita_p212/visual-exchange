//
//  SelleremailViewController.h
//  VisualExchange
//
//  Created by mac on 12/23/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelleremailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *sendemailbtn;
- (IBAction)sendemailbtnpress:(id)sender;
- (IBAction)menubtnpress:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *emaillbl;
@property(strong,nonatomic)NSString *mailstatuscheck;
@end
