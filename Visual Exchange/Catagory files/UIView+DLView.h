//
//  UIView+DLView.h
//  UFeedback
//
//  Created by Moweb_10 on 19/04/16.
//  Copyright © 2016 Moweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DLView)

@property(assign,nonatomic)IBInspectable CGFloat DLCornerRedius;
@property(strong,nonatomic)IBInspectable UIColor *DLBordercolor;
@property(assign,nonatomic)IBInspectable CGFloat DLBorderWidth;
@property(assign,nonatomic)IBInspectable BOOL Iphoneroundmake;
@property(assign,nonatomic)IBInspectable BOOL DLRound;
@property(assign,nonatomic)IBInspectable BOOL IphoneRound;
@property(assign,nonatomic)IBInspectable BOOL FixRound;
@property(assign,nonatomic)IBInspectable BOOL Shadow;
- (void)BottomBoder;
-(void)TopRedius:(CGFloat )cornerredius;
-(void)BottomRedius:(CGFloat )cornerredius;
@end
