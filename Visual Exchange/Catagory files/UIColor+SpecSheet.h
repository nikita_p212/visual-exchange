//
//  UIColor+SpecSheet.h
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (SpecSheet)

+ (UIColor *)specSheetGreen;
+ (UIColor *)colorWithHexString:(NSString *)str;
@end
