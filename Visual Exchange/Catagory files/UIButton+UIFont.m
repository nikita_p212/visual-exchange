//
//  UIButton+UIFont.m
//  BestEmployee
//
//  Created by Moweb_10 on 28/04/16.
//  Copyright © 2016 Moweb_10. All rights reserved.
//

#import "UIButton+UIFont.h"

@implementation UIButton (UIFont)
@dynamic FontAutomatic;
-(void)setFontAutomatic:(BOOL)FontAutomatic{
    
    if(FontAutomatic){
        
         CGFloat height = (self.frame.size.height*SCREEN_WIDTH)/320;
        if(IS_IPAD){
             height = (self.frame.size.height*SCREEN_WIDTH*0.85)/320;
            
        }
       
        
       self.titleLabel.font = [self.titleLabel.font fontWithSize:(height*self.titleLabel.font.pointSize)/self.frame.size.height];
    }
}
@end
