//
//  UIScrollView+DLscroll.m
//  VisualExchange
//
//  Created by Nilay Shah on 28/04/11.
//  Copyright © 1938 Saka Company. All rights reserved.
//

#import "UIScrollView+DLscroll.h"

@implementation UIScrollView (DLscroll)
-(void)AutomaticScrollview{
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.subviews)
        contentRect = CGRectUnion(contentRect, view.frame);
   
    self.contentSize = CGSizeMake(0, contentRect.size.height+450) ;
}
@end
