//
//  UIColor+SpecSheet.m
//  VisualExchange
//
//  Created by Minhaz on 6/19/16.
//  Copyright © 2016 Company. All rights reserved.
//

#import "UIColor+SpecSheet.h"

@implementation UIColor (SpecSheet)

+ (UIColor *)specSheetGreen
{
    return [UIColor colorWithRed:153/255.0 green:193/255.0 blue:58/255.0 alpha:1.0];
}
+ (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    return [UIColor colorWithHex:x];
}
+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}
@end
